﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public AudioSource BossSound;
    public AudioSource BuyingSound;
    public AudioSource UISound;

    public void PlayBossSound()
    {
        BossSound.Play();
    }

    public void PlayBuyingSound()
    {
        BuyingSound.Play();
    }

    public void PlayUISound()
    {
        UISound.Play();
    }

    public void Muted()
    {
        AudioListener.volume = 0;
    }

    public void UnMuted()
    {
        AudioListener.volume = 1;
    }
}
