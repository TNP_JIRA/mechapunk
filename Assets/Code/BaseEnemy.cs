﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
   public int hp;
   
   protected virtual void Init(int hp)
   {
      this.hp = hp;
   }
}
