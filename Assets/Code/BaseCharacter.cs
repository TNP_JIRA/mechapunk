﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class BaseCharacter : MonoBehaviour
{
    public int atk;
    public bool isDead;

    protected virtual void Init(int atk,bool isDead)
    {
        this.atk = atk;
        this.isDead = isDead;
    }
}
