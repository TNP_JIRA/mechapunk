﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Hero1 Char1;
    [SerializeField] private Hero1 Char2;
    [SerializeField] private Hero1 Char3;
    [SerializeField] private Enemy1 Boss1;
    [SerializeField] private Button upgradeButton1;
    [SerializeField] private Button upgradeButton2;
    [SerializeField] private Button upgradeButton3;
    [SerializeField] private Button upgradeButton4;
    [SerializeField] private Button rebirthButton;
    [SerializeField] private int upgrade1Multiplier;
    [SerializeField] private int upgrade2Multiplier;
    [SerializeField] private int upgrade3Multiplier;
    [SerializeField] private int rebirthMultiplier;
    [SerializeField] private HealthBarScript healthBar;
    [SerializeField] private AudioSource BossSound;
    [SerializeField] private AudioSource HeroDieSound;
    [SerializeField] private Image grave1;
    [SerializeField] private Image grave2;
    [SerializeField] private Image grave3;
    public Text bossHPText;
    private int stage = 1;
    public Text stageText;
    private double currentMoney;
    private double totalMoney;
    private double rebirthMoney;
    public Text rebirthMoneyText;
    private double upgradeCostRebirth = 10;
    public Text rebirthText;
    public Text moneyText;
    private int stageMultiplier = 1;
    private double upgradeCost1 = 50;
    public Text upgrade1Text;
    private double upgradeCost2 = 100;
    public Text upgrade2Text;
    private double upgradeCost3 = 150;
    public Text upgrade3Text;
    public Text damageText;
    private int totalDamage;


    private void Start()
    {
        StartGame();
    }

    private void Update()
    {
        DamageCal();
        stageText.text = $"{stage}";
        moneyText.text = $": {totalMoney.ToString("F0")}";
        upgrade1Text.text = $": {upgradeCost1.ToString("F0")}";
        upgrade2Text.text = $": {upgradeCost2.ToString("F0")}";
        upgrade3Text.text = $": {upgradeCost3.ToString("F0")}";
        damageText.text = $"DPS: {totalDamage.ToString("F0")}";
        rebirthText.text = $": {rebirthMoney.ToString("F0")}";
        rebirthMoneyText.text = $"Relic Cost:{upgradeCostRebirth.ToString("F0")}";
    }

    private void StartGame()
    {
        Spawn();
    }
    
    private void Awake()
    {
        upgradeButton1.onClick.AddListener(UpgradeButton1Clicked);
        upgradeButton2.onClick.AddListener(UpgradeButton2Clicked);
        upgradeButton3.onClick.AddListener(UpgradeButton3Clicked);
        upgradeButton4.onClick.AddListener(UpgradeButton4Clicked);
        rebirthButton.onClick.AddListener(Rebirth);
    }
    
    private void UpgradeButton1Clicked()
    {
        var character1 = Char1;
        if (character1.isDead == false)
        { 
            if (totalMoney >= upgradeCost1)
            {
                upgrade1Multiplier += 1;
                totalMoney -= upgradeCost1;
                upgradeCost1 *= 1.2;
                if (stage >= 1000)
                {
                    var number = Random.Range(1, 10);
                    if (number > 8)
                    {
                        character1.isDead = true;
                        character1.atk = 0;
                        Char1.gameObject.SetActive(false);
                        grave1.gameObject.SetActive(true);
                        HeroDieSound.Play();
                    }
                }
            }
        }
    }
    
    private void UpgradeButton2Clicked()
    {
        var character2 = Char2;
        if (character2.isDead == false)
        {
            if (totalMoney >= upgradeCost2)
            {
                upgrade2Multiplier += 1;
                totalMoney -= upgradeCost2;
                upgradeCost2 *= 1.3;
                if (stage >= 1000)
                {
                    var number = Random.Range(1, 10);
                    if (number > 8)
                    {
                        character2.isDead = true;
                        character2.atk = 0;
                        Char2.gameObject.SetActive(false);
                        grave2.gameObject.SetActive(true);
                        HeroDieSound.Play();
                    }
                }
            }
        }
    }
    
    private void UpgradeButton3Clicked()
    {
        var character3 = Char3;
        if (character3.isDead == false)
        {
            if (totalMoney >= upgradeCost3)
            {
                upgrade3Multiplier += 1;
                totalMoney -= upgradeCost3;
                upgradeCost3 *= 1.5;
                if (stage >= 1000)
                {
                    var number = Random.Range(1, 10);
                    if (number > 8)
                    {
                        character3.isDead = true;
                        character3.atk = 0;
                        Char3.gameObject.SetActive(false);
                        grave3.gameObject.SetActive(true);
                        HeroDieSound.Play();
                    }
                }
            }
        }
    }

    private void UpgradeButton4Clicked()
    {
        if (rebirthMoney >= upgradeCostRebirth)
        {
            rebirthMultiplier += 1;
            rebirthMoney -= upgradeCostRebirth;
            upgradeCostRebirth *= 1.5;
        }
    }
    
    private void Spawn()
    {
        var character1 = Char1;
        character1.Init(1,false);
        var character2 = Char2;
        character2.Init(2,false);
        var character3 = Char3;
        character3.Init(3,false);
        var enemy = Boss1;
        enemy.Init(1000);
        healthBar.SetMaxHealth(enemy.hp);
    }
    
    private void DamageCal()
    {
        var character1 = Char1;
        var character2 = Char2;
        var charactor3 = Char3;
        var enemy = Boss1;
        if (enemy.hp > 0)
        {
            totalDamage = (character1.atk * upgrade1Multiplier + character2.atk * upgrade2Multiplier + charactor3.atk * upgrade3Multiplier)*rebirthMultiplier;
            enemy.hp -= totalDamage;
            healthBar.SetHealth(enemy.hp);
        }
        else if (enemy.hp <= 0)
        {
            BossSound.Play();
            MoneyCal();
            stage++;
            stageMultiplier += 2;
            Spawn();
            enemy.hp *= stageMultiplier;
            healthBar.SetMaxHealth(enemy.hp);
        }
        bossHPText.text = $"{enemy.hp}";
    }
    
    private void MoneyCal()
    {
        currentMoney += 10;
        totalMoney =  currentMoney * stageMultiplier;
    }
    
    private void Rebirth()
    {
        var character1 = Char1;
        var character2 = Char2;
        var charactor3 = Char3;
        if(stage > 100)
        {
            rebirthMoney += 2 * stageMultiplier;
            stage = 1;
            totalMoney = 0;
            stageMultiplier = 1;
            upgrade1Multiplier = 1;
            upgrade2Multiplier = 1;
            upgrade3Multiplier = 1;
            totalDamage = character1.atk * upgrade1Multiplier + character2.atk * upgrade2Multiplier + charactor3.atk * upgrade3Multiplier;
            StartGame();
            grave1.gameObject.SetActive(false);
            grave2.gameObject.SetActive(false);
            grave3.gameObject.SetActive(false);
            Char1.gameObject.SetActive(true);
            Char2.gameObject.SetActive(true);
            Char3.gameObject.SetActive(true);
        }
    }
}
